package com.warehouse.obj;

import java.util.List;
import java.util.Vector;

import com.warehouse.obj.thread.Warehouseman;

/**
 * 
 * @author barto
 *
 */
public class Ramp extends AbstWait {

	private static final int MAX_WAREHOUSEMANS = 2;

	private List<Warehouseman> warehousemans = new Vector<>();
	private Truck truck;

	public synchronized void addWarehouseman(Warehouseman warehouseman) {
		System.out.println("<<< Add Warehouseman to Ramp " + Thread.currentThread().getName() + ">>>");
		warehousemans.add(warehouseman);
	}

	public synchronized void removeWarehouseman(Warehouseman warehouseman) {
		System.out.println(">>> Remove Warehouseman from Ramp take 5 sec <<<");
		try {
			Thread.sleep(1000 * 5);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		warehousemans.remove(warehouseman);
		System.out.println(">>>> Warehouseman has Removed <<<<");
		notifyAllSync();
	}



	public synchronized Boolean isMaxWarehousemansOnRampIfNotAdd(Warehouseman men) {
		System.out.println("<<>> Warehousemans on ramp: " + warehousemans.size() + " <<>>");
		if (warehousemans.size() < MAX_WAREHOUSEMANS) {
			addWarehouseman(men);
			return Boolean.FALSE;
		}

		return Boolean.TRUE;
	}

	public synchronized Boolean isTruckOnRamp() {
		return this.truck != null;
	}

	public synchronized Truck getTruck() {
		return this.truck;
	}

	public void changeTruck(Truck truck) {
		this.truck = truck;
	}

	public synchronized void removeTruckFromRampIfCan() {
		if (truck != null && truck.isTruckStateEnds()) {
			removeTruckFromRamp();
		}
	}

	public synchronized void removeTruckFromRamp() {
		System.out.println("<<---- Removing Truck from Ramp ---->>");
		this.truck = null;
	}
	
	public synchronized boolean isTruckForUnloadingOnRamp() {
		return truck != null && TruckType.UNLOADING.equals(truck.getType());
	}

	public synchronized boolean isTruckForLoadingOnRamp() {
		return truck != null &&  TruckType.LOADING.equals(truck.getType());
	}

}
