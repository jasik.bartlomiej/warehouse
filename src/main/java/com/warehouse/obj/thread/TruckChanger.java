package com.warehouse.obj.thread;

import com.warehouse.obj.Warehouse;

public class TruckChanger implements Runnable {

	private Warehouse warehouse;

	public TruckChanger(Warehouse warehouse) {
		this.warehouse = warehouse;
	}

	@Override
	public void run() {
		warehouse.servTruckChange();
	}

}
