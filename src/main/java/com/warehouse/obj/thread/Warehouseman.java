package com.warehouse.obj.thread;

import com.warehouse.obj.Warehouse;

/**
 * 
 * @author barto
 *
 */
public class Warehouseman implements Runnable {
	
	private Warehouse warehouse;
	
	public Warehouseman(Warehouse warehouse) {
		this.warehouse = warehouse;
	}
	
	@Override
	public void run() {
		warehouse.workWarehousemen(this);
	}
	
}
