package com.warehouse.obj;

import java.util.List;
import java.util.Vector;

import org.springframework.stereotype.Component;

import com.warehouse.lib.MustWaitException;
import com.warehouse.obj.thread.Warehouseman;

/**
 * 
 * @author barto
 *
 */
@Component
public class Warehouse extends AbstWait {

	private Ramp ramp = new Ramp();
	private Bookcase bookcase = new Bookcase();
	private List<Truck> trucks = createTrucks();
	private int servedTruckCunter = 0;

	private List<Truck> createTrucks() {
		List<Truck> trucks = new Vector<>();
		trucks.add(new Truck(TruckType.UNLOADING));
		trucks.add(new Truck(TruckType.UNLOADING));
		trucks.add(new Truck(TruckType.LOADING));
		return trucks;
	}

	public Bookcase getBookcase() {
		return bookcase;
	}

	public Ramp getRamp() {
		return ramp;
	}

	public List<Truck> getTrucks() {
		return trucks;
	}

	private void changeTruckOnRamp() {
		if (trucks.size() > servedTruckCunter) {
			ramp.changeTruck(trucks.get(servedTruckCunter));
			System.out.println("___***___Changing Truck On Ramp___***___ current truck: " + servedTruckCunter + " - "
					+ ramp.getTruck().getType() + " amount: " + ramp.getTruck().getAmount());
			notifyAllSync();
		}
		servedTruckCunter++;
	}

	private boolean isThereStillTruckToServiced() {
		return trucks.size() >= servedTruckCunter;
	}

	public void servTruckChange() {
		System.out.println("______Run Truck_Changer______");
		while (isThereStillTruckToServiced()) {
			System.out.println("______There is Still Truck To Serviced______");
			getRamp().removeTruckFromRampIfCan();

			if (!getRamp().isTruckOnRamp()) {
				System.out.println("___!!!___There is no Truck On ramp___!!!___");
				changeTruckOnRamp();
			} else {
				System.out
						.println("___zzz___TruckChanger is waiting until truck will be loading or unloading___zzz___");
				try {
					Thread.sleep(1000 * 10);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		getRamp().removeTruckFromRamp();
		System.out.println("________FINISHED________TRUCK_______");
		notifyAllSync();
	}

	public void workWarehousemen(Warehouseman men) {
		while (isThereStillTruckToServiced()) {
			System.out.println("Run " + Thread.currentThread().getName());
			try {
				if (getRamp().isTruckOnRamp() && !getRamp().getTruck().isTruckStateEnds()) {
					try {
						makeAction(men);
					} catch (Exception e) {
						throw new MustWaitException();
					}
				} else {
					throw new MustWaitException();
				}
			} catch (MustWaitException mwe) {
				mustWait();
			}
		}
		System.out.println("________FINISHED________WAREHOUSEMAN_______");
		notifyAllSync();
	}

	synchronized void mustWait() {
		System.out.println("!!! WAIT !!! " + Thread.currentThread().getName() + " !!!! WAIT !!!! ");
		try {
			wait();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private void makeAction(Warehouseman men) {
		if (getRamp().isTruckForUnloadingOnRamp())
			takeFromTruckIfCan(men);

		if (getRamp().isTruckForLoadingOnRamp())
			putToTruckIfCan(men);
	}

	private void putToTruckIfCan(Warehouseman men) {
		if (getRamp().isTruckOnRamp() && !getRamp().getTruck().isTruckStateEnds()
				&& !getRamp().isMaxWarehousemansOnRampIfNotAdd(men)) {
			bookcase.take();
			System.out.println("$$$ " + Thread.currentThread().getName() + "  -1- start puting to TRUCK-- ");
			getRamp().getTruck().put();
			getRamp().removeWarehouseman(men);
			System.out.println("$$$ " + Thread.currentThread().getName() + "  -2- end puting to TRUCK-- ");
		} else {
			throw new MustWaitException();
		}
	}

	private void takeFromTruckIfCan(Warehouseman men) {
		if (getRamp().isTruckOnRamp() && !getRamp().getTruck().isTruckStateEnds()
				&& !getRamp().isMaxWarehousemansOnRampIfNotAdd(men)) {
			System.out.println("$$$ " + Thread.currentThread().getName() + "  -1- start taking from TRUCK-- ");
			getRamp().getTruck().take();
			getRamp().removeWarehouseman(men);
			System.out.println("$$$ " + Thread.currentThread().getName() + "  -2- end taking from TRUCK-- ");
			bookcase.put();
			System.out.println("$$$ " + Thread.currentThread().getName() + "  -3- end putting on BOOKCASE-- ");
		} else {
			throw new MustWaitException();
		}
	}

}
