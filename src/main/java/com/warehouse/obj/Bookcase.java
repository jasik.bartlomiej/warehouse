package com.warehouse.obj;

import com.warehouse.lib.MustWaitException;

/**
 * 
 * @author barto
 *
 */
public class Bookcase extends AbstWait {

	private int amount;

	private static final int MIN_AMOUNT = 0;

	public synchronized void put() {
		try {
			int random = (int) (Math.random() * 40 + 20);
			System.out.println("^^^^ " + Thread.currentThread().getName() + " need " + random
					+ " sec for put to bookcase");
			Thread.sleep(1000 * random);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		amount++;
		System.out.println("^^^^ current amount on BOOKCASE: " + amount + " ^^^^ ");
	}

	public synchronized void take() {
		if (amount > MIN_AMOUNT) {
			amount--;
			System.out.println("^^^^ " + Thread.currentThread().getName() + " take form BOOKCASE, current amount: "
					+ amount + " ^^^^ ");
		} else {
			throw new MustWaitException();
		}
	}

}
