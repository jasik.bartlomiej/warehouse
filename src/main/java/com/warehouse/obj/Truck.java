package com.warehouse.obj;

import com.warehouse.lib.MustWaitException;

/**
 * 
 * @author barto
 *
 */
public class Truck {

	private TruckType type;
	private int amount;
	private TruckState truckState = TruckState.WATING;

	private static int MAX_AMOUNT = 50;
	private static int MIN_AMOUNT = 0;

	public Truck(TruckType type) {
		this.type = type;
		if (TruckType.LOADING.equals(type)) {
			this.amount = MIN_AMOUNT;
		} else {
			this.amount = MAX_AMOUNT;
		}
	}

	public synchronized void take() {
		try {
			int random = (int )(Math.random() * 10 + 5);
			System.out.println("----- "+Thread.currentThread().getName() + " need " + random + " sec to take stuff from truck...");
			Thread.sleep(1000 * random);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		if(amount <= MIN_AMOUNT) {
			throw new MustWaitException();
		}
		amount--;
		System.out.println(".....AFTER TAKE...... Current amount in truck on ramp: " + getAmount() + " ####");
		changeStateIfNeed(MIN_AMOUNT);
	}

	private synchronized void changeStateIfNeed(int amount) {
		if (TruckState.WATING.equals(truckState)) {
			truckState = TruckState.IN_PROCESS;
		}
		if (this.amount == amount) {
			truckState = TruckState.ENDS;
			notifyAll();
		}
	}

	public synchronized void put() {
		System.out.println("----- "+Thread.currentThread().getName() + " put to truck...");
		if(amount >= MAX_AMOUNT) {
			throw new MustWaitException();
		}
		amount++;
		System.out.println(".....AFTER PUT...... Current amount in truck on ramp: " + getAmount() + " ####");
		changeStateIfNeed(MAX_AMOUNT);
	}

	public synchronized TruckType getType() {
		return type;
	}

	public synchronized int getAmount() {
		return amount;
	}

	public synchronized TruckState getTruckState() {
		return truckState;
	}
	
	public synchronized Boolean isTruckStateEnds() {
		return TruckState.ENDS.equals(truckState);
	}

}
