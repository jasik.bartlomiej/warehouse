package com.warehouse;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.warehouse.obj.Warehouse;
import com.warehouse.obj.thread.TruckChanger;
import com.warehouse.obj.thread.Warehouseman;

@Service
public class RunService {

	@Autowired
	Warehouse warehouse;

	public void run() {
		System.out.println("--->> Start program <<---");

		Thread truckChanger = new Thread(new TruckChanger(warehouse), "Truck_Changer");
		truckChanger.setPriority(Thread.MAX_PRIORITY);
		truckChanger.start();

		List<Runnable> warehousemanList = new ArrayList<>();
		IntStream.range(0, 10).forEach(i -> {
			Thread t = new Thread(new Warehouseman(warehouse), "Warehousemen_" + i);
			t.start();
			warehousemanList.add(t);
		});

		
		System.out.println("<<<--- Ends program --->>>");
	}

}
